from django.urls import path
from . import views

app_name = 'matakuliah'

urlpatterns = [
    path('create_matkul/', views.create_matakuliah_view, name='create_matkul'),
    path('', views.matakuliah, name='matakuliah'),
    path('delete_matkul/', views.delete_matkul, name='delete_matkul'),
    path('detail/<str:id>', views.detail_matkul_view, name='detail_matkul'),
]
