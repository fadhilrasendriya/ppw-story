from django.test import TestCase, Client
from django.urls.base import resolve
from . import views

# Create your tests here.
class AccordionTest(TestCase):

    def test_url_exists(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_function(self):
        found = resolve('/accordion/')
        self.assertEqual(found.func, views.index)


