from django.test import TestCase, Client
from django.urls.base import resolve
from django.contrib.auth.models import User

from . import views
# Create your tests here.


class UserTestCase(TestCase):

    def test_login_url_exists(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code, 200)

    def test_register_url_exists(self):
        response = Client().get('/user/register/')
        self.assertEqual(response.status_code, 200)

    def test_login_views(self):
        found = resolve('/user/login/')
        self.assertEqual(found.func, views.log_in)

    def test_register_views(self):
        found = resolve('/user/register/')
        self.assertEqual(found.func, views.register)

    def test_register_success(self):
        users = User.objects.all()
        original_count = users.count()
        data = {'username': 'toru', 'password1': 'notchill', 'password2': 'notchill', 'email': 'toru@ppwmail.com'}
        response = Client().post('/user/register/', data=data)

        self.assertEqual(response.status_code, 302)
        count = User.objects.all().count()
        self.assertEqual(original_count + 1, count)





    def test_register_fail(self):
        users = User.objects.all()
        original_count = users.count()
        data = {'username': 'toru', 'password1': 'notchill1', 'password2': 'notchill2', 'email': 'toru@ppwmail.com'}
        response = Client().post('/user/register/', data=data)

        count = User.objects.all().count()
        self.assertEqual(original_count, count)

    def registerDummy(self):
        data = {'username': 'toru', 'password1': 'notchill', 'password2': 'notchill', 'email': 'toru@ppwmail.com'}
        Client().post('/user/register/', data=data)

    def test_login_success(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        response = Client().post('/user/login/', data=data)
        self.assertEqual(response.status_code, 302)



    def test_login_fail(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill1'}
        response = Client().post('/user/login/', data=data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Invalid username or password")

    def loginDummy(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        Client().post('/user/login/', data=data)


    def test_logout_url_exists(self):
        self.loginDummy()
        response = Client().get('/user/logout/')
        self.assertEqual(response.status_code, 302)

    def test_logout_func(self):
        found = resolve('/user/logout/')
        self.assertEqual(found.func, views.log_out)


    def test_login_greeting(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        response = self.client.post('/user/login/', data=data, follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, toru', html_response)

    def test_logout_succesful(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/user/login/', data=data)
        response = self.client.get('/user/logout/', follow=True)
        html_response = response.content.decode('utf8')
        self.assertNotIn('Hello, toru', html_response)


