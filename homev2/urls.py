from django.urls import path
from . import views

app_name = 'homev2'

urlpatterns = [
    path('', views.index, name='index'),
    path('gallery/', views.gallery, name='gallery'),
]
