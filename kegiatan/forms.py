from django import forms
from .models import Todo, Person

class TodoForm(forms.ModelForm):

    class Meta:
        model = Todo
        fields = "__all__"

class PersonForm(forms.ModelForm):

    class Meta:
        model = Person
        fields = "__all__"
