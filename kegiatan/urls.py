from . import views
from django.urls import path

app_name = 'kegiatan'

urlpatterns = [
    path('', views.index, name='index'),
    path('add-todo/', views.add_todo, name='add_todo'),
    path('add-person/', views.add_person, name='add_person'),
]
