from django.shortcuts import render, redirect, get_object_or_404
from .forms import TodoForm, PersonForm
from .models import Todo, Person
# Create your views here.


def index(request):
    list_kegiatan = Todo.objects.all()
    people = Person.objects.all()
    context = {'list_kegiatan': list_kegiatan, 'people': people}
    person_form = PersonForm()
    context['person_form'] = person_form
    return render(request, 'kegiatan/index.html', context)


def add_todo(request):
    form = TodoForm()
    context = {}
    if request.method == 'POST':
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('/kegiatan/')
    else:
        context['form'] = form
        return render(request, 'kegiatan/add_todo.html', context)


def add_person(request):

    if request.method == 'POST':
        form = PersonForm(request.POST)
        if form.is_valid():
            form.save()
    return redirect('/kegiatan/')
