(function($) {

  var allPanels = $('.accordion > .sectionss > dd').hide();

  $('.accordion > .sectionss > dt > a').click(function() {
      var content = $(this).parent().next().next().next();
      if (content.is(':hidden')) {
          allPanels.slideUp();
          content.slideDown();
      } else {
          allPanels.slideUp();
      }


    return false;
  });

})(jQuery);

$(document).ready(function(){

  // jQuery methods go here...
  $(".upButton,.downButton").click(function(){
        var row = $(this).parents(".sectionss");
        if ($(this).is(".upButton")) {
            row.insertBefore(row.prev());
        } else if ($(this).is(".downButton")) {
            row.insertAfter(row.next());
        }
    });

});